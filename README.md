# Elixir : plus qu'un langage, une plateforme

Elixir fournit un outillage et une syntaxe modernes donnant accès à la
plateforme Erlang/OTP. Cette plateforme robuste implémente un certain nombre de
concepts intéressants qui sont peu présents dans les écosystème plus répandus.
Pattern matching, concurrence par passage de messages immutables et arbres de
supervision sont autant de caractéristiques singulières qui font de Elixir un
outil de choix pour le web temps réel, les pipelines de traitement de données
ou même l'embarqué.

Je tâcherai lors de cette présentation de vous transmettre l'esprit de cette
plateforme et de vous faire entrevoir ses possibilités.

## Support

[Le support interactif](Support.ipynb) est au format Jupyter Notebook. Pour
l'exécuter vous aurez besoin de [Jupyter](https://jupyter.org/), du
[kernel pour Elixir](https://github.com/pprzetacznik/IElixir) et de
[Elixir lui-même](https://elixir-lang.org/install.html).

Il y a aussi un [version statique du support](Support.slides.html) que vous
pouvez consulter dans n'importe quel navigateur.

## Projet d'exemple

Le projet d'exemple est disponible à https://gitlab.com/amarandon/chat-example
